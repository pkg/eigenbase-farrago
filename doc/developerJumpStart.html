<html>

<head>
<title>Farrago Developer Jumpstart</title>
<link rel="stylesheet" type="text/css" href="stylesheet.css" />

</head>

<body>

<h1>Getting Started With Farrago</h1>

This document provides instructions on how to build and run Farrago
and its dependencies (including Fennel).

<h2>Supported Platforms</h2>

So far, the Fennel C++ library only builds and runs on Linux and
Windows.  This means that if you want to get a fully functional
Farrago build, you have to be running one of these operating systems.
However, the Java portion is likely to run on other platforms where
JDK 1.5 is available.  You can use it to test parsing, validation,
optimization, and the metadata aspects of DDL, as well as queries
against foreign data accessible via Java (a sample <a
href="http://hsqldb.sf.net">HSQLDB</a> source is provided for testing
JDBC, and the <a href="howto/medmdr.html">MDR HOWTO</a> explains
MDR/JMI usage).

<p>

So, if you're working on a task that doesn't require Fennel, you can
do the development anywhere.  If you need Fennel, then you have a few
options:

<ul>

<li>use your own Linux or Windows box if you have one (Linux is strongly
recommended; the Windows C++ build/debug environment is slow and touchy)

<li>use one of the Sourceforge compile farm machines (Fennel has
been successfully tested on some of them)

<li>work on porting Fennel to your platform!

</ul>

<h3>Important Windows Note</h3>

If you are using Windows, please read and follow the <a
href="cygwinSetup.html">Cygwin setup instructions</a> before going any
further.  

<h2>Get the Source Code</h2>

There are two ways to get the Farrago source code; either
<a href="http://www.eigenbase.org/download.html">
download a full Eigenbase source release</a>
or pull the latest code from the Eigenbase Perforce server.
(If you just want to take a look-see through the codebase,
you can use <a href="http://p4web.eigenbase.org/open/dev/farrago/">
p4web</a>.)  

<p>

If you have downloaded a full Eigenbase source release, you can skip the
rest of this section about Perforce and continue on with 
<a href="#InitBuild">Initial Build</a>; note that in this case 
we'll refer to the top-level directory created when you unpacked the
release archive as <code>dev</code> in this and other developer docs.

<p>

If you're new to Perforce, you might want to read the official <a
href="http://www.perforce.com/perforce/doc.022/manuals/boilerplates/quickstart.html">
Perforce Quick Start</a>.  If you don't already have a p4 client
program, download the appropriate binary from
<a href="http://www.perforce.com/perforce/loadprog.html">
Perforce</a> (you only need the client, not the server).

<p>

There are a number of ways to configure Perforce depending on the
platform (on Windows through the GUI, on Unix through environment
variables or a .p4env file).  The essential information you need to
provide is:

<ul>
<li>P4PORT=perforce.eigenbase.org:1666
<li>P4USER=your-sourceforge-user-name
<li>P4CLIENT=your-user-name.your-hostname
</ul>

In order to make changes, you'll need to send your username to the
Farrago project admin and request write access.  If you don't have a
Sourceforge account yet, <a href="http://sf.net/account/register.php">
go get one</a>.  For read-only clients (like if you just want a
snapshot of the latest code), use the predefined username "guest" (the
server license has a user limit, but there's no limit on the number of
clients, so feel free to create multiple workspaces on as many
machines as needed).

<p>

Once you've got Perforce set up, run <code>p4 client</code>
(or edit your default clientspec from P4WIN).  
Map your view to match this example:

<pre><code>
View:
        //open/dev/thirdparty/... //YOURCLIENT/dev/thirdparty/...
        //open/dev/fennel/... //YOURCLIENT/dev/fennel/...
        //open/dev/farrago/... //YOURCLIENT/dev/farrago/...

</code></pre>

We'll refer to the <code>YOURCLIENT/dev</code> local filesystem directory
as <code>dev</code> in this and other developer docs.

<p>

While editing your client, you should also set the
<code>LineEnd</code> attribute to <code>share</code>, especially if
you are developing on Windows or using Samba.  This prevents DOS
CRLF's from getting checked in.

<p>

Finally, run <code>p4 sync</code> to get all the source files and
thirdparty distributions into your client workspace.  This may take a
while because some of the thirdparty distributions are large.  

<a name="InitBuild">
<h2>Initial Build</h2>
</a>

Before proceeding, make sure you have <a
href="http://java.sun.com/j2se/1.5">JDK 1.5</a> or higher installed,
with JAVA_HOME set to the JDK installation root.

<p>

For the first build in a new sandbox, there's a lot of
one-time setup that needs to be done, including building some
of the third-party components from source.  This process is automated
by the script <code>dev/farrago/initBuild.sh</code>.  Change to the
<code>dev/farrago</code> directory and run the initial build as follows:

<pre><code>
initBuild.sh --with-fennel

  or

initBuild.sh --without-fennel

</code></pre>

If you're building without Fennel, this should be fairly quick (under
ten minutes on most machines); with Fennel it will take quite a bit
longer.  If all goes well, you'll be left with a complete Farrago
build for your platform.  Otherwise, you'll need to dig into the
script and its output to see what's going wrong.  If you can't figure
it out, ask for help on the <a
href="http://sourceforge.net/mail/?group_id=80183">farrago-developers
mailing list</a>.  You should probably include the last 100 lines of
build output including the error.

<h3>Fennel and Autotools</h3>

If you are building with Fennel, the initBuild script will check to
see if you have a autotools versions (autoconf, automake, and libtool)
which are known to work; if you don't, the script will fail before
starting the C++ build.  If this occurs, you can either follow the
directions in the script error message to build and install the
autotools from thirdparty, or update your machine using the mechanism
provided by your OS distribution (Linux or Cygwin).

<p>

If you need help with problems in the C++ portion of the build, use
the <a href="http://sourceforge.net/mail/?group_id=53161">fennel-developers
mailing list</a>.

<h2>Environment Variables</h2>

The initial build script takes care of setting some necessary
environment variables internally, but once it completes they are left
unset.  For subsequent runs and builds of Farrago, you will need to
make sure these are set.  Scripts are provided for this purpose.  You
can run them automatically from a login script such as
<code>.bash_profile</code>, or run them manually before working with
Farrago:

<pre><code>
source dev/fennel/fennelenv.sh dev/fennel
source dev/farrago/farragoenv.sh dev/thirdparty

</code></pre>

As usual, you only need the Fennel script if Fennel support is
enabled.  Don't execute these scripts directly; you must use
<code>.</code> or <code>source</code> in order for the environment
variable settings to take effect for subsequent commands.

<h2>Incremental Builds</h2>

Once you already have a working build, updating to the latest code
(assuming you're using the Perforce server) requires following these
steps:

<ol>

<li>
<code>p4 sync</code>.  You'll probably want to examine the output to
see what has changed (or you can ignore it and blindly carry out the
rest of the steps unconditionally).

<li>
If anything under <code>dev/thirdparty/...</code> has changed, cd to
that directory and run <code>make all</code>.  New
thirdparty components and upgrades to new versions are fairly rare.
Upgrades to C++ components usually require you to repeat the original
initBuild.sh process.

<li>
If anything under <code>dev/fennel/...</code> has changed (and you're
building with Fennel support), cd to that directory and run
<code>make</code>.  If it has been a long time since your last sync,
you may get errors, in which case you should run <code>make
clean</code> and then try again.  If you still get errors, see below
on forcing a full rebuild.

<li>
cd to <code>dev/farrago</code> and run <code>ant test</code> to make
sure you'll be working from a good build (WARNING: this will rebuild
your catalog automatically, destroying any data in your Farrago
database).  To avoid running the tests, run <code>ant createCatalog</code>
instead.

</ol>

If none of the C++ components under thirdparty have been upgraded, and
you'd like to force a rebuild of everything else, skip the steps after
the sync and instead run

<pre><code>
cd dev/farrago
initBuild.sh --with-fennel --skip-fennel-thirdparty-build

</code></pre>

This will recapitulate the original full build, but skipping the
time-consuming thirdparty C++ components.

<h2>Catalog Dump</h2>

At any time, you can get a dump of the catalog by running <code>ant
exportCatalog</code>; the output will be an XMI file in
<code>dev/farrago/catalog/xmi/FarragoCatalogExport.xmi</code>.
This can be useful for debugging the effects of DDL
execution.

<h2>Catalog Backup/Restore</h2>

The <code>ant createCatalog</code> task (invoked automatically as part
of <code>ant test</code>) creates a backup of the initial catalog (and
database if Fennel is enabled).  If your catalog gets corrupted, you
can recover with <code>ant restoreCatalog</code>.  This is a lot
faster than <code>ant createCatalog</code> (which is another way to
get back to a good state).

<h2>Interactive SQL</h2>

You can execute interactive SQL from a command-line via the
open-source JDBC <a href="http://sqlline.sourceforge.net">sqlline</a>
project.  Just run the <code>sqllineEngine</code> script from dev/farrago.
After connecting, try <code>select * from sales.emps;</code>
(but if you disabled Fennel, don't be disappointed when the result set
comes back empty).

<p>

If you prefer using a GUI, try the <code>dbmanEngine</code> script instead.

<h2>Tracing</h2>

See <a href="http://wiki.eigenbase.org/FarragoTracing">the wiki docs</a>
for instructions on tracing Farrago.

<h2>Unit Testing</h2>

See <a href="http://wiki.eigenbase.org/FarragoTests">the wiki docs</a>
for information on Farrago unit tests.

<h2>Debugging</h2>

The <a href="http://wiki.eigenbase.org/FarragoDevFAQ">developer
FAQ</a> contains some useful information on debugging, including
how to set up Eclipse.

<p>

A standalone debugger (<a
href="http://www.bluemarsh.com/java/jswat">JSwat</a>) is also provided
in the thirdparty distribution.  Run <code>ant jswat.sqlline</code> or
<code>ant jswat.isql</code> to use this debugger for execution of
interactive SQL commands.  For sqlline, input/output is performed
through the JSwat Output pane at the bottom left of the screen.  The
<code>junitDebug</code> script can also be used to debug one unit test
at a time.  If you want to use another debugger, the
<code>jswatDebug</code> ant macro definition in
<code>dev/farrago/buildMacros.xml</code> might be helpful as a
template for passing the right startup information.  Also, as part of
a successful build, the full classpath is always dumped to the file
<code>dev/farrago/classpath.gen</code>.

<p>

<h2>Client/Server Operation</h2>

So far, all of the execution modes discussed have been <em>embedded
engine mode</em>, where the Farrago engine is loaded directly into a
single process such as sqlline.  This is the most
useful for debugging, but scripts are also available to test
client/server mode.  Run the <code>farragoServer</code> script to
start the server in a shell (after startup, it waits listening for
connections; enter the <code>!quit</code> command in that shell to
shut it down).  From another shell, run <code>sqllineClient</code> or
<code>dbmanClient</code> to connect and execute commands.

<h2>Coding</h2>

Please read the <a
href="http://wiki.eigenbase.org/CodingConventions">coding
conventions</a> documentation.  Before starting development projects,
consider using <a href="privateBranch.html">private branching</a>.

<a name="Feedback">
<h2>Feedback</h2>
</a>

If you have trouble with any of this, or have other questions, use
the <a href="http://sourceforge.net/mail/?group_id=80183">
farrago-developers mailing list</a>.  

</body>

</html>
