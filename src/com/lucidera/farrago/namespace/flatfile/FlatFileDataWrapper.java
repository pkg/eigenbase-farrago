/*
// $Id: //open/dev/farrago/src/com/lucidera/farrago/namespace/flatfile/FlatFileDataWrapper.java#9 $
// Farrago is an extensible data management system.
// Copyright (C) 2005-2009 LucidEra, Inc.
// Copyright (C) 2005-2009 The Eigenbase Project
// Portions Copyright (C) 2003-2009 John V. Sichi
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version approved by The Eigenbase Project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package com.lucidera.farrago.namespace.flatfile;

/**
 * This package is obsolete and scheduled for deletion.
 *
 * @author John V. Pham
 * @version $Id: //open/dev/farrago/src/com/lucidera/farrago/namespace/flatfile/FlatFileDataWrapper.java#9 $
 *
 * @deprecated use net.sf.farrago.namespace.flatfile.FlatFileDataWrapper instead
 */
public class FlatFileDataWrapper
    extends net.sf.farrago.namespace.flatfile.FlatFileDataWrapper
{
    public FlatFileDataWrapper()
    {
        super();
    }
}

// End FlatFileDataWrapper.java
