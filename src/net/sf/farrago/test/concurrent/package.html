<html>
<head>
<title>Package net.sf.farrago.test.concurrent</title>
</head>
<body>

Contains concurrency tests for Farrago.

<table border="1" width="100%">
  <tr>
    <th>Revision</th>
    <td>$Id: //open/dev/farrago/src/net/sf/farrago/test/concurrent/package.html#7 $</td>
  </tr>
  <tr>
    <th>Copyright</th>
    <td>Copyright (C) 2005-2009 The Eigenbase Project
    <br>Copyright (C) 2005-2009 SQLstream, Inc.
    <br>Copyright (C) 2005-2009 LucidEra, Inc.</td>
  </tr>
  <tr>
    <th>Author</th>
    <td>Stephan Zuercher</td>
  </tr>
</table>

<hr>

<h2>Farrago Scripted Concurrency Testing</h2>

Farrago Scripted Concurrency Testing is based on
{@link net.sf.farrago.test Farrago Unit Testing}.

<p>

<h3>Scripts</h3>

<p>

As in the single-threaded diff-based testing, the test writers will
need to concern themselves with 3 files:

<ul>
	<li>the test script itself (e.g. <code>unitsql/foo.mtsql</code>)</li>
	<li>the reference output file (e.g. <code>unitsql/foo.ref</code>)</li>
	<li>the test output file (e.g. <code>unitsql/foo.log</code>)</li>
</ul>

<p>

The multi-threaded case the test script ends in <code>.mtsql</code> to
make it easier distinguish it from single-threaded test scripts.

<p>

Keywords in the script denote special actions to be taken by the test
harness (repeating a sequence of SQL statements, setting a query
timeout, etc.).  Keywords start with an &#64;.

<p>

The multi-threaded test script is divided into sections, one for test
setup (e.g. SQL that creates schemas, tables, etc. that must be in
place before the multi-threaded testing begins), and one for each
thread in the test.  The setup section can be omitted in its entirety
if it's not needed.  Here's an example test script:

<p>
<pre>&#64;nolockstep

&#64;setup
	CREATE SCHEMA s;
	CREATE TABLE s.t (c INTEGER NOT NULL PRIMARY KEY);
&#64;end

&#64;thread first_thread
	select * from s.t where c &lt; 5;
&#64;end

&#64;thread second_thread
	select * from s.t where c &gt;= 5;
&#64;end</pre>

<h4>Keywords</h4>

<table style="border: solid #000000 1px;" cellspacing="1" cellpadding="1" border="1">
<tr>
	<th align="left">Keyword</th>
	<th align="left">Description</th>
</tr>

<tr>
	<td>&#64;lockstep<br />&#64;nolockstep</td>
	<td>
		Controls whether execution of each command in the
		script is synchronized.  These keywords must appear at
		the start of the file (before <tt>&#64;setup</tt> or
		<tt>&#64;thread</tt>), or not at all.  If neither keyword
		is present, the default is <tt>&#64;nolockstep</tt>.
	</td>
</tr>

<tr>
	<td>&#64;disabled<br />&#64;enabled</td>
	<td>
		Controls whether execution of the test is disabled or
		not.  These keywords must appear at the start of the
		file (before <tt>&#64;setup</tt> or <tt>&#64;thread</tt>), or
		not at all.  If neither keyword is present, the
		default is <tt>&#64;enabled</tt>.
	</td>
</tr>

<tr>
	<td>&#64;setup<br /> ...<br />&#64;end</td>
	<td>
		Defines SQL statements that will execute before the
		multi-threaded portion of the test is started.
		Optional.
	</td>
</tr>

<tr>
	<td nowrap>&#64;thread X[,Y,...]<br />...<br />&#64;end</td>
	<td>
		Defines one or more threads of execution and their SQL
		commands.  Note that if multiple thread names (X, Y,
		...) are given, multiple threads will execute the same
		commands.  At least one <tt>&#64;thread</tt> block must
		appear in the script.  If only one appears, you should
		probably be using the old single-threaded testing.
	</td>
</tr>

<tr>
	<td nowrap>&#64;repeat N<br />...<br />&#64;end</td>
	<td>
		Specifies that the given commands should be repeated
		with N times.  May only appear inside of a
		<tt>&#64;thread</tt> block.
	</td>
</tr>

<tr>
	<td>&#64;sync</td>
	<td>
		Specifies that the given thread should block and wait
		for all other threads to reach their <tt>&#64;sync</tt>
		commands.  The same number of <tt>&#64;sync</tt> keywords
		must appear in each thread once all loops have been
		unrolled.  <tt>&#64;sync</tt> can only be used with
		<tt>&#64;nolockstep</tt>.  May only appear inside of a
		<tt>&#64;thread</tt> or <tt>&#64;repeat</tt> block.
	</td>
</tr>

<tr>
	<td nowrap>&#64;timeout T SQL</td>
	<td>
		Execute the given SQL with a query timeout of T
		milliseconds.  The statement is prepared and executed,
		all results are fetched and written to the
		<tt>.ref</tt> file, and the statement is closed.  May
		only appear inside of a <tt>&#64;thread</tt> or
		<tt>&#64;repeat</tt> block.
	</td>
</tr>

<tr>
	<td nowrap>&#64;prepare SQL<br />&#64;fetch [T]<br />&#64;close</td>
	<td>
		Prepare the given SQL.  Execute and fetch the results
		with an optional query timeout of T milliseconds,
		writing them into the <tt>.ref</tt> file.  Close the
		statement.  Only the <tt>&#64;sync</tt> or <tt>&#64;sleep</tt>
		keywords may appear between these three keywords.  May
		only appear inside of a <tt>&#64;thread</tt> or
		<tt>&#64;repeat</tt> block.
	</td>
</tr>

<tr>
	<td nowrap>&#64;sleep T</td>
	<td>
		Sleep for T milliseconds.  May only appear inside of a
		<tt>&#64;thread</tt> or <tt>&#64;repeat</tt> block.
	</td>
</tr>

<tr>
	<td nowrap>&#64;err SQL</td>
	<td>
		Execute SQL and verify that it results in an
		error.
	</td>
</tr>
</table>

<p>

SQL statements can appear anywhere within the <tt>&#64;setup</tt> and
<tt>&#64;thread</tt> blocks.  Any statement beginning with <tt>select</tt>
is executed as a JDBC query.  Statements beginning with
<tt>insert</tt>, <tt>update</tt> or <tt>delete</tt> are executed as
JDBC updates.  Everything else is executed as DDL.  Whitespace is
ignored at the beginning and end of a statement.  Statements are
delimited with semicolons, even when preceded by a keyword.  Keywords
must be the first thing (except for whitespace) that appers on a line.
Double dashes indicate comments.  For example:

<pre>select * from sales.depts;                 -- good

&#64;timeout 5000 select * from sales.depts;   -- good

&#64;timeout 5000 select *
from sales.depts;                          -- good

&#64;timeout 5000 select * from sales.depts    -- bad, missing semicolon
select * from sales.emps;

select * from sales.depts                  -- bad, missing semicolon
&#64;timeout 5000 select * from sales.emps;

select * from sales.depts; &#64;sync           -- bad, sync must be on next line
</pre>

<h4>Example Script</h4>

<pre>-- redundant:
&#64;nolockstep

-- Two threads reading the same data.
&#64;thread 1,2
	-- pre execute the SQL to prime the pumps
	&#64;timeout 1000 select * from sales.bids;

	&#64;prepare select * from sales.bids;

	-- rendezvous with writer thread
	&#64;sync
	&#64;fetch 15000
	&#64;sync
	&#64;close	
&#64;end

&#64;thread writer
	-- rendezvous with reader threads
	&#64;sync
	&#64;sleep 5000
	insert into sales.bids
		values(1,  'ORCL', 100, 12.34,     10000, 'Oracle at 12.34');
	commit;
	insert into sales.bids
		values(2,  'MSFT', 101, 23.45,     20000, 'Microsoft at 23.45');
	commit;

	-- real test has more inserts here

	&#64;sync
&#64;end</pre>

<h3>Example Output File</h3>

The output from each thread is stored in a temporary file until the
test completes.  At that point, the files are merged together into a
single <tt>.log</tt> file containing the results of each thread, in
the order the threads were defined.  The output for the example script
looks like:

<pre>-- thread 1
&gt; select * from sales.bids;
+---------+------------+
| DEPTNO  |    NAME    |
+---------+------------+
| 10      | Sales      |
| 20      | Marketing  |
| 30      | Accounts   |
+---------+------------+
&gt;
&gt; select * from sales.bids;
+---------+------------+
| DEPTNO  |    NAME    |
+---------+------------+
| 10      | Sales      |
| 20      | Marketing  |
| 30      | Accounts   |
+---------+------------+

-- end of thread 1

-- thread 2
&gt; select * from sales.bids;
+---------+------------+
| DEPTNO  |    NAME    |
+---------+------------+
| 10      | Sales      |
| 20      | Marketing  |
| 30      | Accounts   |
+---------+------------+
&gt;
&gt; select * from sales.bids;
+---------+------------+
| DEPTNO  |    NAME    |
+---------+------------+
| 10      | Sales      |
| 20      | Marketing  |
| 30      | Accounts   |
+---------+------------+

-- end of thread 2

-- thread writer
&gt; insert into sales.bids
&gt;	values(1,  'ORCL', 100, 12.34,     10000, 'Oracle at 12.34');
1 row affected.
&gt; commit;
&gt; insert into sales.bids
&gt;	values(2,  'MSFT', 101, 23.45,     20000, 'Microsoft at 23.45');
1 row affected.
&gt; commit;
-- end of thread writer</pre>

(Yes the results of the select statements are obviously wrong.)

<h3>Open Issues</h3>

<ul>
	<li>Repeating tests for a period of time isn't supported.</li>
</ul>

</body>
</html>
