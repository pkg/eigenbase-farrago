/*
// $Id: //open/dev/farrago/src/net/sf/farrago/server/FarragoRmiJdbcServer.java#7 $
// Farrago is an extensible data management system.
// Copyright (C) 2006-2009 The Eigenbase Project
// Copyright (C) 2006-2009 SQLstream, Inc.
// Copyright (C) 2006-2009 LucidEra, Inc.
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version approved by The Eigenbase Project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package net.sf.farrago.server;

import java.io.*;

import java.util.*;

import net.sf.farrago.jdbc.engine.*;


/**
 * FarragoRmiJdbcServer is a wrapper which configures an RmiJdbc server to
 * listen for connections on behalf of a Farrago DBMS engine.
 *
 * @author John V. Sichi
 * @version $Id: //open/dev/farrago/src/net/sf/farrago/server/FarragoRmiJdbcServer.java#7 $
 */
public class FarragoRmiJdbcServer
    extends FarragoAbstractServer
{
    //~ Constructors -----------------------------------------------------------

    /**
     * Creates a new FarragoRmiJdbcServer instance, with console output to
     * System.out. This constructor can be used to embed a FarragoRmiJdbcServer
     * inside of another container such as a J2EE app server.
     */
    public FarragoRmiJdbcServer()
    {
        super();
    }

    /**
     * Creates a new FarragoRmiJdbcServer instance, with redirected console
     * output. This constructor can be used to embed a FarragoServer inside of
     * another container such as a J2EE app server.
     *
     * @param pw receives console output
     */
    public FarragoRmiJdbcServer(PrintWriter pw)
        throws Exception
    {
        super(pw);
    }

    //~ Methods ----------------------------------------------------------------

    /**
     * Defines the main entry point for the Farrago server. Customized servers
     * can provide their own which call start() with an extended implementation
     * of {@link net.sf.farrago.jdbc.engine.FarragoJdbcServerDriver}.
     *
     * @param args ignored
     */
    public static void main(String [] args)
        throws Exception
    {
        FarragoRmiJdbcServer server = new FarragoRmiJdbcServer();
        server.start(new FarragoJdbcEngineDriver());
        server.runConsole();
    }

    // implement FarragoAbstractServer
    protected int startNetwork(FarragoJdbcServerDriver jdbcDriver)
        throws Exception
    {
        List<String> argList = new ArrayList<String>();

        if (rmiRegistry != null) {
            // A server instance was previously in existence, so don't
            // try to recreate the RMI registry.
            argList.add("-noreg");
        }

        argList.add("-port");
        argList.add(Integer.toString(rmiRegistryPort));

        if (singleListenerPort != -1) {
            argList.add("-lp");
            argList.add(Integer.toString(singleListenerPort));
        }

        FarragoRJJdbcServer.main(argList.toArray(new String[argList.size()]));
        locateRmiRegistry();

        return rmiRegistryPort;
    }
}

// End FarragoRmiJdbcServer.java
