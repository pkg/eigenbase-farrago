/*
// $Id: //open/dev/farrago/src/net/sf/farrago/namespace/impl/MedAbstractBase.java#11 $
// Farrago is an extensible data management system.
// Copyright (C) 2005-2009 The Eigenbase Project
// Copyright (C) 2005-2009 SQLstream, Inc.
// Copyright (C) 2005-2009 LucidEra, Inc.
// Portions Copyright (C) 2003-2009 John V. Sichi
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version approved by The Eigenbase Project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package net.sf.farrago.namespace.impl;

import net.sf.farrago.plugin.*;


/**
 * MedAbstractBase is an abstract base for classes used to build data wrappers.
 *
 * @author John V. Sichi
 * @version $Id: //open/dev/farrago/src/net/sf/farrago/namespace/impl/MedAbstractBase.java#11 $
 */
public class MedAbstractBase
    extends FarragoAbstractPluginBase
{
    // NOTE jvs 17-May-2004: At the moment there's nothing MED-specific here,
    // but it's staying around as a placeholder in case there is something in
    // the future.  Put any generic plugin stuff in FarragoAbstractPluginBase.
}

// End MedAbstractBase.java
