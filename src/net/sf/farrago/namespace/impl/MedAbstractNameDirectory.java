/*
// $Id: //open/dev/farrago/src/net/sf/farrago/namespace/impl/MedAbstractNameDirectory.java#14 $
// Farrago is an extensible data management system.
// Copyright (C) 2005-2009 The Eigenbase Project
// Copyright (C) 2005-2009 SQLstream, Inc.
// Copyright (C) 2005-2009 LucidEra, Inc.
// Portions Copyright (C) 2003-2009 John V. Sichi
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation; either version 2 of the License, or (at your option)
// any later version approved by The Eigenbase Project.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package net.sf.farrago.namespace.impl;

import java.sql.*;

import java.util.*;

import net.sf.farrago.catalog.*;
import net.sf.farrago.fem.med.*;
import net.sf.farrago.fem.sql2003.*;
import net.sf.farrago.namespace.*;


/**
 * MedAbstractNameDirectory is an abstract base class for implementations of the
 * {@link FarragoMedNameDirectory} interface.
 *
 * @author John V. Sichi
 * @version $Id: //open/dev/farrago/src/net/sf/farrago/namespace/impl/MedAbstractNameDirectory.java#14 $
 */
public abstract class MedAbstractNameDirectory
    extends MedAbstractBase
    implements FarragoMedNameDirectory
{
    //~ Methods ----------------------------------------------------------------

    // implement FarragoMedNameDirectory
    public FarragoMedNameDirectory lookupSubdirectory(String foreignName)
        throws SQLException
    {
        return null;
    }

    // implement FarragoMedNameDirectory
    public boolean queryMetadata(
        FarragoMedMetadataQuery query,
        FarragoMedMetadataSink sink)
        throws SQLException
    {
        return false;
    }

    // implement FarragoMedNameDirectory
    public FemBaseColumnSet newImportedColumnSet(
        FarragoRepos repos,
        String tableName)
    {
        // By default, all imported objects are treated as foreign tables
        FemBaseColumnSet newColumnSet = repos.newFemForeignTable();
        newColumnSet.setModality(ModalityTypeEnum.MODALITYTYPE_RELATIONAL);
        return newColumnSet;
    }
}

// End MedAbstractNameDirectory.java
